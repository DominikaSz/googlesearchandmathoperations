﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace GoogleSearchAndMathOperations_Test
{
	[TestFixture]
	public class MathOperations : BaseMethod
	{
		#region [ADDITION TESTS]

		[TestCase(4, 92, "96")]
		[TestCase(888, 964, "1852")]
		[TestCase(373735, 359, "374094")]
		[TestCase(41351, 3339, "44690")]
		public void AdditionInt_CorrectResult(int number1, int number2, string expectedResult)
		{
			string operations = String.Concat(number1, "+", number2);

			OpenBrowserFindElemend(operations);
			
			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) =>{return d.Title.ToLower().StartsWith(operations);});

			IWebElement resultAdditionField = webDriver.FindElement(By.Id("cwos"));

			string resultAddition = resultAdditionField.Text;

			Assert.AreEqual(expectedResult, resultAddition);
			webDriver.Quit();
		}

		[TestCase(3634, 92, "9666")]
		[TestCase(345, 765, "33")]
		[TestCase(85568, 34, "372464094")]
		[TestCase(3636, 67447, "547")]
		public void AdditionInt_IncorrectResult(int number1, int number2, string expectedResult)
		{
			string operations = String.Concat(number1, "+", number2);

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultAdditionField = webDriver.FindElement(By.Id("cwos"));

			string resultAddition = resultAdditionField.Text;

			Assert.AreNotEqual(expectedResult, resultAddition);
			webDriver.Quit();
		}

		[TestCase(43.54, 92.65, "136.19")]
		[TestCase(121.22, 964.5, "1085.72")]
		[TestCase(373735.85, 3539.06, "377274.91")]
		[TestCase(44.64, 3339.12, "3383.76")]
		public void AdditionDouble_CorrectResult(double number1, double number2, string expectedResult)
		{
			string operations = String.Concat(number1, "+", number2);

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultAdditionField = webDriver.FindElement(By.Id("cwos"));

			string resultAddition = resultAdditionField.Text;

			Assert.AreEqual(expectedResult, resultAddition);
			webDriver.Quit();
		}

		[TestCase(43.54, 92.65, "163")]
		[TestCase(121.22, 964.5, "104.2")]
		[TestCase(373735.85, 3539.06, "3772.5")]
		[TestCase(44.64, 3339.12, "3336336383.76")]
		public void AdditionDouble_IncorrectResult(double number1, double number2, string expectedResult)
		{
			string operations = String.Concat(number1, "+", number2);

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultAdditionField = webDriver.FindElement(By.Id("cwos"));

			string resultAddition = resultAdditionField.Text;

			Assert.AreNotEqual(expectedResult, resultAddition);
			webDriver.Quit();
		}

		#endregion

		#region [SUBSTRACT TESTS]

		[TestCase(4655, 92, "4563")]
		[TestCase(8388, 964, "7424")]
		[TestCase(373735, 359, "373376")]
		[TestCase(41351, 3339, "38012")]
		public void SubtractInt_CorrectResult(int number1, int number2, string expectedResult)
		{
			string operations = String.Concat(number1, "-", number2);

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultSubtractField = webDriver.FindElement(By.Id("cwos"));

			string resultSubtract = resultSubtractField.Text;

			Assert.AreEqual(expectedResult, resultSubtract);
			webDriver.Quit();
		}

		[TestCase(3634, 92, "9666")]
		[TestCase(345, 765, "33")]
		[TestCase(85568, 34, "372464094")]
		[TestCase(3636, 67447, "547")]
		public void SubtractInt_IncorrectResult(int number1, int number2, string expectedResult)
		{
			string operations = String.Concat(number1, "-", number2);

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultSubtractField = webDriver.FindElement(By.Id("cwos"));

			string resultSubtract = resultSubtractField.Text;

			Assert.AreNotEqual(expectedResult, resultSubtract);
			webDriver.Quit();
		}

		[TestCase(93.54, 92.67, "0.87")]
		[TestCase(1121.22, 964.5, "156.72")]
		[TestCase(373735.85, 3539.06, "370196.79")]
		[TestCase(4411.64, 3339.12, "1072.52")]
		public void SubtractDouble_CorrectResult(double number1, double number2, string expectedResult)
		{
			string operations = String.Concat(number1, "-", number2);

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultSubtractField = webDriver.FindElement(By.Id("cwos"));

			string resultSubtract = resultSubtractField.Text;

			Assert.AreEqual(expectedResult, resultSubtract);
			webDriver.Quit();
		}

		[TestCase(43.54, 92.65, "163")]
		[TestCase(121.22, 964.5, "104.2")]
		[TestCase(373735.85, 3539.06, "3772.5")]
		[TestCase(44.64, 3339.12, "3336336383.76")]
		public void SubtractDouble_IncorrectResult(double number1, double number2, string expectedResult)
		{
			string operations = String.Concat(number1, "-", number2);

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultSubtractField = webDriver.FindElement(By.Id("cwos"));

			string resultSubtract = resultSubtractField.Text;

			Assert.AreNotEqual(expectedResult, resultSubtract);
			webDriver.Quit();
		}

		#endregion

		#region [MULTIPLICATION TESTS]

		[TestCase(2, 6, "12")]
		[TestCase(345, 345, "119025")]
		[TestCase(567, 54, "30618")]
		[TestCase(23, 344, "7912")]
		public void MultiplicationInt_CorrectResult(int number1, int number2, string expectedResult)
		{
			string operations = String.Concat(number1, "*", number2);

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultMultiplicationField = webDriver.FindElement(By.Id("cwos"));

			string resultMultiplication = resultMultiplicationField.Text;

			Assert.AreEqual(expectedResult, resultMultiplication);
			webDriver.Quit();
		}

		[TestCase(3634, 92, "9666")]
		[TestCase(345, 765, "33")]
		[TestCase(85568, 34, "372464094")]
		[TestCase(3636, 67447, "547")]
		public void MultiplicationInt_IncorrectResult(int number1, int number2, string expectedResult)
		{
			string operations = String.Concat(number1, "*", number2);

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultMultiplicationField = webDriver.FindElement(By.Id("cwos"));

			string resultMultiplication = resultMultiplicationField.Text;

			Assert.AreNotEqual(expectedResult, resultMultiplication);
			webDriver.Quit();
		}

		[TestCase(13.54, 12.65, "171.281")]
		[TestCase(1321.22, 9624.5, "12716081.89")]
		[TestCase(434.85, 34.06, "14810.991")]
		[TestCase(3.64, 2, "7.28")]
		public void MultiplicationDouble_CorrectResult(double number1, double number2, string expectedResult)
		{
			string operations = String.Concat(number1, "*", number2);

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultMultiplicationField = webDriver.FindElement(By.Id("cwos"));

			string resultMultiplication = resultMultiplicationField.Text;

			Assert.AreEqual(expectedResult, resultMultiplication);
			webDriver.Quit();
		}

		[TestCase(43.54, 92.65, "163")]
		[TestCase(121.22, 964.5, "104.2")]
		[TestCase(373735.85, 3539.06, "3772.5")]
		[TestCase(44.64, 3339.12, "3336336383.76")]
		public void MultiplicationDouble_IncorrectResult(double number1, double number2, string expectedResult)
		{
			string operations = String.Concat(number1, "*", number2);

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultMultiplicationField = webDriver.FindElement(By.Id("cwos"));

			string resultMultiplication = resultMultiplicationField.Text;

			Assert.AreNotEqual(expectedResult, resultMultiplication);
			webDriver.Quit();
		}

		#endregion

		#region [DIVISION TESTS]

		[TestCase(644, 92, "7")]
		[TestCase(10604, 964, "11")]
		[TestCase(22734, 11367, "2")]
		[TestCase(3168, 33, "96")]
		public void DivisionInt_CorrectResult(int number1, int number2, string expectedResult)
		{
			string operations = String.Concat(number1, "/", number2);

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultDivisionField = webDriver.FindElement(By.Id("cwos"));

			string resultDivision = resultDivisionField.Text;

			Assert.AreEqual(expectedResult, resultDivision);
			webDriver.Quit();
		}

		[TestCase(3634, 92, "9666")]
		[TestCase(345, 765, "33")]
		[TestCase(85568, 34, "372464094")]
		[TestCase(3636, 67447, "547")]
		public void DivisionInt_IncorrectResult(int number1, int number2, string expectedResult)
		{
			string operations = String.Concat(number1, "/", number2);

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultDivisionField = webDriver.FindElement(By.Id("cwos"));

			string resultDivision = resultDivisionField.Text;

			Assert.AreNotEqual(expectedResult, resultDivision);
			webDriver.Quit();
		}

		[TestCase(43.54, 92.65, "0.4699406368")]
		[TestCase(121.22, 964.5, "0.12568170036")]
		[TestCase(373735.85, 3539.06, "105.603140382")]
		[TestCase(44.64, 3339.12, "0.01336879177")]
		public void DivisionDouble_CorrectResult(double number1, double number2, string expectedResult)
		{
			string operations = String.Concat(number1, "/", number2);

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultDivisionField = webDriver.FindElement(By.Id("cwos"));

			string resultDivision = resultDivisionField.Text;

			Assert.AreEqual(expectedResult, resultDivision);
			webDriver.Quit();
		}

		[TestCase(43.54, 92.65, "163")]
		[TestCase(121.22, 964.5, "104.2")]
		[TestCase(373735.85, 3539.06, "3772.5")]
		[TestCase(44.64, 3339.12, "3336336383.76")]
		public void DivisionDouble_IncorrectResult(double number1, double number2, string expectedResult)
		{
			string operations = String.Concat(number1, "/", number2);

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultDivisionField = webDriver.FindElement(By.Id("cwos"));

			string resultDivision = resultDivisionField.Text;

			Assert.AreNotEqual(expectedResult, resultDivision);
			webDriver.Quit();
		}

		#endregion

		#region [LOOK FOR METHODS TESTS]

		[TestCase("Mnożenie", "Mnożenie - Szukaj w Google")]
		[TestCase("Dzielenie", "Dzielenie - Szukaj w Google")]
		[TestCase("Dodawanie", "Dodawanie - Szukaj w Google")]
		[TestCase("Odejmowanie", "Odejmowanie - Szukaj w Google")]
		public void LookForMathOperations_CorrectResult(string typeMathOperation, string expectedResult)
		{
			OpenBrowserFindElemend(typeMathOperation);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) =>
			{
				return d.Title.ToLower().StartsWith(typeMathOperation.ToLower());
			});

			string webTitle = webDriver.Title;

			Assert.AreEqual(expectedResult, webTitle);
			webDriver.Quit();
		}

		[TestCase("Dodawanie", "Mnożenie - Szukaj w Google")]
		[TestCase("Odejmowanie", "Dzielenie - Szukaj w Google")]
		[TestCase("Dzielenie", "Dodawanie - Szukaj w Google")]
		[TestCase("Mnożenie", "Odejmowanie - Szukaj w Google")]
		public void LookForMathOperations_IncorrectResult(string typeMathOperation, string expectedResult)
		{
			OpenBrowserFindElemend(typeMathOperation);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) =>
			{
				return d.Title.ToLower().StartsWith(typeMathOperation.ToLower());
			});

			string webTitle = webDriver.Title;

			Assert.AreNotEqual(expectedResult, webTitle);
			webDriver.Quit();
		}

		#endregion 

		#region [SQRT TESTS]

		[TestCase(4, "2")]
		[TestCase(6084, "78")]
		[TestCase(321489, "567")]
		[TestCase(1709905201, "41351")]
		public void SqrtInt_CorrectResult(int number, string expectedResult)
		{
			string operations = String.Concat("sqrt(", number, ")");

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultSqrtField = webDriver.FindElement(By.Id("cwos"));

			string resultSqrt = resultSqrtField.Text;

			Assert.AreEqual(expectedResult, resultSqrt);
			webDriver.Quit();
		}

		[TestCase(3634, "9666")]
		[TestCase(345, "33")]
		[TestCase(85568, "372464094")]
		[TestCase(3636, "547")]
		public void SqrtInt_IncorrectResult(int number, string expectedResult)
		{
			string operations = String.Concat("sqrt(", number, ")");

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultSqrtField = webDriver.FindElement(By.Id("cwos"));

			string resultSqrt = resultSqrtField.Text;

			Assert.AreNotEqual(expectedResult, resultSqrt);
			webDriver.Quit();
		}

		[TestCase(43.54, "6.59848467453")]
		[TestCase(121.22, "11.0099954587")]
		[TestCase(373735.85, "611.339390192")]
		[TestCase(44.64, "6.6813172354")]
		public void SqrtDouble_CorrectResult(double number, string expectedResult)
		{
			string operations = String.Concat("sqrt(", number, ")");

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultSqrtField = webDriver.FindElement(By.Id("cwos"));

			string resultSqrt = resultSqrtField.Text;

			Assert.AreEqual(expectedResult, resultSqrt);
			webDriver.Quit();
		}

		[TestCase(43.54, "163")]
		[TestCase(121.22, "104.2")]
		[TestCase(373735.85, "3772.5")]
		[TestCase(44.64, "3336336383.76")]
		public void SqrtDouble_IncorrectResult(double number, string expectedResult)
		{
			string operations = String.Concat("sqrt(", number, ")");

			OpenBrowserFindElemend(operations);

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith(operations); });

			IWebElement resultSqrtField = webDriver.FindElement(By.Id("cwos"));

			string resultSqrt = resultSqrtField.Text;

			Assert.AreNotEqual(expectedResult, resultSqrt);
			webDriver.Quit();
		}

		#endregion

		#region [OTHER METHODS]

		public void OpenBrowserFindElemend(string textToSearch)
		{
			webDriver = new ChromeDriver();
			webDriver.Navigate().GoToUrl(url);
			IWebElement webSearch = webDriver.FindElement(By.Name("q"));
			webSearch.SendKeys(textToSearch);
			webSearch.Submit();
		}

		#endregion
	}
}
