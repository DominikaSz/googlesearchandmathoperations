﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace GoogleSearchAndMathOperations_Test
{
	[TestClass]
	public class GoogleSearch : BaseMethod
	{
		#region [TESTS]

		[TestMethod]
		public void Search()
		{
			IWebElement webSearch = webDriver.FindElement(By.Name("q"));
			webSearch.SendKeys("Zamek Rajsko");
			webSearch.Submit();

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith("zamek rajsko"); });
		}

		[TestMethod]
		public void SearchEmptyString()
		{
			IWebElement webSearch = webDriver.FindElement(By.Name("q"));
			webSearch.SendKeys("");
			webSearch.Submit();
		}

		[TestMethod]
		public void CheckTitle_CorrectResult()
		{
			IWebElement webSearch = webDriver.FindElement(By.Name("q"));
			webSearch.SendKeys("Pogoda 2016");
			webSearch.Submit();

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith("pogoda 2016"); });

			string webTitle = webDriver.Title;

			Assert.AreEqual("Pogoda 2016 - Szukaj w Google", webTitle);
		}

		[TestMethod]
		public void CheckTitle_IncorrectResult()
		{
			IWebElement webSearch = webDriver.FindElement(By.Name("q"));
			webSearch.SendKeys("Pogoda 2016");
			webSearch.Submit();

			WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(1));
			wait.Until((d) => { return d.Title.ToLower().StartsWith("pogoda 2016"); });

			string webTitle = webDriver.Title;

			Assert.AreNotEqual("Pogoda w roku 2016 w Polsce", webTitle);
		}

		[TestMethod]
		public void CheckIfTimeSearchIsLessThan250Milliseconds_CorrectResult()
		{
			var watch = System.Diagnostics.Stopwatch.StartNew();

			IWebElement webSearch = webDriver.FindElement(By.Name("q"));
			webSearch.SendKeys("Zamek Rajsko");
			webSearch.Submit();

			watch.Stop();
			var timeSearch = watch.ElapsedMilliseconds;

			Assert.IsTrue(timeSearch < 250, "The search time was greater than 250 millisecond");
		}

		[TestMethod]
		public void CheckIfTimeSearchIsMoreThan10Milliseconds_IncorrectResult()
		{
			var watch = System.Diagnostics.Stopwatch.StartNew();

			IWebElement webSearch = webDriver.FindElement(By.Name("q"));
			webSearch.SendKeys("Zamek Rajsko");
			webSearch.Submit();

			watch.Stop();
			var timeSearch = watch.ElapsedMilliseconds;

			Assert.IsFalse(timeSearch < 10, "The search time was not greater than 10 millisecond");
		}

		[TestMethod]
		public void SearchSeveralTimes()
		{
			SearchSeveralTimesMethod();
		}

		[TestMethod]
		public void CheckIfTimeSearchSeveralTimesIsLessThan550Milliseconds_CorrectResult()
		{
			var watch = System.Diagnostics.Stopwatch.StartNew();
            
            SearchSeveralTimesMethod();
            watch.Stop();
			var timeSearch = watch.ElapsedMilliseconds;

			Assert.IsTrue(timeSearch < 550, "The search time was greater than 550 millisecond");
		}

		[TestMethod]
		public void CheckIfTimeSearchSeveralTimesIsMoreThan10Milliseconds_IncorrectResult()
		{
			var watch = System.Diagnostics.Stopwatch.StartNew();
            
			SearchSeveralTimesMethod();
			watch.Stop();
			var timeSearch = watch.ElapsedMilliseconds;

			Assert.IsFalse(timeSearch < 10, "The search time was not greater than 10 millisecond");
		}
		
		#endregion

		#region [OTHER METHODS]

		public void SearchSeveralTimesMethod()
		{
			IWebElement webSearch = webDriver.FindElement(By.Name("q"));
			webSearch.SendKeys("Polska");
			webSearch.Submit();

			webSearch.Clear();
			webSearch = webDriver.FindElement(By.Name("q"));
			webSearch.SendKeys("Niemcy");
			webSearch.Submit();

			webSearch.Clear();
			webSearch = webDriver.FindElement(By.Name("q"));
			webSearch.SendKeys("Hiszpania");
			webSearch.Submit();

			webSearch.Clear();
			webSearch = webDriver.FindElement(By.Name("q"));
			webSearch.SendKeys("Belgia");
			webSearch.Submit();
		}

		#endregion
	}
}
