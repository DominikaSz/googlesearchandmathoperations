﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;

namespace GoogleSearchAndMathOperations_Test
{
	[TestClass]
	public class BaseMethod
	{
		public IWebDriver webDriver;
		public string url = "http://google.pl/";

		[TestInitialize]
		public void TestSetup()
		{
			webDriver = new ChromeDriver();
			webDriver.Navigate().GoToUrl(url);
		}

		[TestCleanup]
		public void Cleanup()
		{
			webDriver.Quit();
		}

		[TestMethod]
		public void SearchInGoogle(string textToSearch)
		{
			IWebElement webSearch = webDriver.FindElement(By.Name("q"));
			webSearch.SendKeys(textToSearch);
			webSearch.Submit();
		}
	}
}
